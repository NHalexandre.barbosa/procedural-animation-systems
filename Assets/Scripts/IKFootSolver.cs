using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Sirenix.OdinInspector;

namespace Ac1dwir3.ProceduralAnimation
{
    public class IKFootSolver : MonoBehaviour
    {
        //Too limited, Only works when legs are offset from each other (one foot back, other foot forward), Seems to only have two legs in mind
        #region Unity Prototype Series Method
        /*[SerializeField] Transform body;
        [SerializeField] IKFootSolver otherLegs;

        [FoldoutGroup("Step Stats")][SerializeField, SuffixLabel("m", true), MinValue(0.01f)] float stepLength = 0;
        [FoldoutGroup("Step Stats")][SerializeField, SuffixLabel("m", true), MinValue(0f)] float stepHeight = 0;
        [FoldoutGroup("Step Stats")][SerializeField, SuffixLabel("m/s", true), MinValue(0.01f)] float stepSpeed = 0;
        [FoldoutGroup("Step Stats")][ReadOnly, ShowInInspector] float stepProgress = 1;


        [FoldoutGroup("Raycast Info")][SerializeField, SuffixLabel("m", true)] float raycastDistance;
        [FoldoutGroup("Raycast Info")][SerializeField] LayerMask groundMask;
        [FoldoutGroup("Raycast Info")][ReadOnly, ShowInInspector] Vector3 raycastOffset;
        Ray groundFinderRay;
        RaycastHit hit;

        [FoldoutGroup("Debugging")][ReadOnly, ShowInInspector] Vector3 currentPosition;
        [FoldoutGroup("Debugging")][ReadOnly, ShowInInspector] Vector3 previousPosition;
        [FoldoutGroup("Debugging")][ReadOnly, ShowInInspector] Vector3 desiredPosition;

        private void Awake()
        {
            raycastOffset = transform.localPosition + new Vector3(0, 0.25f, 0);
            stepLength = transform.root.localScale.x / 10f * 2f;
            currentPosition = transform.position;
        }

        void Update()
        {
            transform.position = currentPosition;

            groundFinderRay = new Ray(body.position + raycastOffset, Vector3.down);
            if (Physics.Raycast(groundFinderRay, out hit, raycastDistance, groundMask))
            {
                if (Vector3.Distance(desiredPosition, hit.point) > stepLength)
                {
                    stepProgress = 0;
                    desiredPosition = hit.point;
                }
            }

            if(stepProgress < 1)
            {
                Vector3 footPosition = Vector3.Lerp(previousPosition, desiredPosition, stepProgress);
                footPosition.y += Mathf.Sin(stepProgress * Mathf.PI) * stepHeight;

                currentPosition = footPosition;
                stepProgress += Time.deltaTime * stepSpeed;
            }
            else
            {
                previousPosition = desiredPosition;
            }
        }

        private void OnDrawGizmos()
        {
            //Leg IK Target
            Gizmos.color = Color.blue;
            Gizmos.DrawCube(transform.position, Vector3.one * 0.025f);

            //New Position
            Gizmos.color = Color.red;
            Gizmos.DrawSphere(desiredPosition, 0.05f);

            //Step Distance
            Gizmos.color = new Color(1, 0, 1); //Purple
            Gizmos.DrawWireSphere(transform.position, stepLength);
        }*/
        #endregion
    }
}