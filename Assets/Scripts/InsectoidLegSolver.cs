using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Ac1dwir3.ProceduralAnimation
{
    public class InsectoidLegSolver : MonoBehaviour
    {
        //Too Janky, Doesn't work when not on the ground (Jumping/Falling), 
        #region L�o Chaumartin Method
        /*public Transform[] legTargets;
        public float stepSize = 1f;
        public int smoothness = 1;
        public float stepHeight = 0.1f;
        public bool bodyOrientation = true;

        private float raycastRange = 1f;
        private Vector3[] defaultLegPositions;
        private Vector3[] lastLegPositions;
        private Vector3 lastBodyUp;
        private bool[] legMoving;
        private int nbLegs;

        private Vector3 velocity;
        private Vector3 lastVelocity;
        private Vector3 lastBodyPos;

        private float velocityMultiplier = 15f;

        static Vector3[] MatchToSurfaceFromAbove(Vector3 point, float halfRange, Vector3 up)
        {
            Vector3[] result = new Vector3[2];
            RaycastHit hit;
            Ray ray = new Ray(point + halfRange * up, -up);

            if (Physics.Raycast(ray, out hit, 2f * halfRange))
            {
                result[0] = hit.point;
                result[1] = hit.normal;
            }
            else
            {
                result[0] = point;
            }
            return result;
        }

        void Start()
        {
            lastBodyUp = transform.up;

            nbLegs = legTargets.Length;
            defaultLegPositions = new Vector3[nbLegs];
            lastLegPositions = new Vector3[nbLegs];
            legMoving = new bool[nbLegs];
            for (int i = 0; i < nbLegs; ++i)
            {
                defaultLegPositions[i] = legTargets[i].localPosition;
                lastLegPositions[i] = legTargets[i].position;
                legMoving[i] = false;
            }
            lastBodyPos = transform.position;
        }

        IEnumerator PerformStep(int index, Vector3 targetPoint)
        {
            Vector3 startPos = lastLegPositions[index];
            for (int i = 1; i <= smoothness; ++i)
            {
                legTargets[index].position = Vector3.Lerp(startPos, targetPoint, i / (float)(smoothness + 1f));
                legTargets[index].position += transform.up * Mathf.Sin(i / (float)(smoothness + 1f) * Mathf.PI) * stepHeight;
                yield return new WaitForFixedUpdate();
            }
            legTargets[index].position = targetPoint;
            lastLegPositions[index] = legTargets[index].position;
            legMoving[0] = false;
        }


        void FixedUpdate()
        {
            velocity = transform.position - lastBodyPos;
            velocity = (velocity + smoothness * lastVelocity) / (smoothness + 1f);

            if (velocity.magnitude < 0.000025f)
                velocity = lastVelocity;
            else
                lastVelocity = velocity;


            Vector3[] desiredPositions = new Vector3[nbLegs];
            int indexToMove = -1;
            float maxDistance = stepSize;
            for (int i = 0; i < nbLegs; ++i)
            {
                desiredPositions[i] = transform.TransformPoint(defaultLegPositions[i]);

                float distance = Vector3.ProjectOnPlane(desiredPositions[i] + velocity * velocityMultiplier - lastLegPositions[i], transform.up).magnitude;
                if (distance > maxDistance)
                {
                    maxDistance = distance;
                    indexToMove = i;
                }
            }
            for (int i = 0; i < nbLegs; ++i)
                if (i != indexToMove)
                    legTargets[i].position = lastLegPositions[i];

            if (indexToMove != -1 && !legMoving[0])
            {
                Vector3 targetPoint = desiredPositions[indexToMove] + Mathf.Clamp(velocity.magnitude * velocityMultiplier, 0.0f, 1.5f) * (desiredPositions[indexToMove] - legTargets[indexToMove].position) + velocity * velocityMultiplier;
                Vector3[] positionAndNormal = MatchToSurfaceFromAbove(targetPoint, raycastRange, transform.up);
                legMoving[0] = true;
                StartCoroutine(PerformStep(indexToMove, positionAndNormal[0]));
            }

            lastBodyPos = transform.position;
            if (nbLegs > 3 && bodyOrientation)
            {
                Vector3 v1 = legTargets[0].position - legTargets[1].position;
                Vector3 v2 = legTargets[2].position - legTargets[3].position;
                Vector3 normal = Vector3.Cross(v1, v2).normalized;
                Vector3 up = Vector3.Lerp(lastBodyUp, normal, 1f / (float)(smoothness + 1));
                transform.up = up;
                lastBodyUp = up;
            }
        }

        private void OnDrawGizmosSelected()
        {
            for (int i = 0; i < nbLegs; ++i)
            {
                Gizmos.color = Color.red;
                Gizmos.DrawWireSphere(legTargets[i].position, 0.05f);
                Gizmos.color = Color.green;
                Gizmos.DrawWireSphere(transform.TransformPoint(defaultLegPositions[i]), stepSize);
            }
        }*/
        #endregion

        #region My Method V1
        /*[SerializeField] Transform body;

        [SerializeField, Required] Transform[] legIKTargets;

        [FoldoutGroup("Step Stats")][SerializeField, SuffixLabel("m", true), MinValue(0.01f)] float stepLength;
        [FoldoutGroup("Step Stats")][SerializeField, SuffixLabel("m", true), MinValue(0.01f)] float stepHeight;
        [FoldoutGroup("Step Stats")][SerializeField, SuffixLabel("m/s", true), MinValue(0.01f)] float stepSpeed;
        [FoldoutGroup("Step Stats")][ReadOnly, ShowInInspector] float stepProgress = 0;


        [FoldoutGroup("Raycast Info")][SerializeField, SuffixLabel("m", true)] float raycastDistance;
        [FoldoutGroup("Raycast Info")][SerializeField] LayerMask groundMask;
        [FoldoutGroup("Raycast Info")][ReadOnly, ShowInInspector] Vector3[] raycastOffsets = null;
        Ray groundFinderRay;
        RaycastHit hit;

        //[FoldoutGroup("Position Data")][ReadOnly, ShowInInspector] Vector3[] defaultPositions = null;
        [FoldoutGroup("Position Data")][ReadOnly, ShowInInspector] Vector3[] desiredPositions = null;
        [FoldoutGroup("Position Data")][ReadOnly, ShowInInspector] Vector3[] currentPositions = null;
        [FoldoutGroup("Position Data")][ReadOnly, ShowInInspector] Vector3[] previousPositions = null;
        [FoldoutGroup("Position Data")][ReadOnly, ShowInInspector] bool[] isMoving = null;
        [FoldoutGroup("Position Data")][ReadOnly, ShowInInspector] Vector3 velocity;
        [FoldoutGroup("Position Data")][ReadOnly, ShowInInspector] Vector3 lastVelocity;
        [FoldoutGroup("Position Data")][ReadOnly, ShowInInspector] Vector3 lastBodyPosition;

        private void Awake()
        {
            stepLength = transform.localScale.x / 10f * 2f;
            stepHeight = transform.localScale.x / 10f;

            raycastOffsets = new Vector3[legIKTargets.Length];

            //defaultPositions = new Vector3[legIKTargets.Length];
            desiredPositions = new Vector3[legIKTargets.Length];
            currentPositions = new Vector3[legIKTargets.Length];
            previousPositions = new Vector3[legIKTargets.Length];
            isMoving = new bool[legIKTargets.Length];
            for (int i = 0; i < legIKTargets.Length; i++)
            {
                raycastOffsets[i] = legIKTargets[i].localPosition + new Vector3(0, .25f, 0);

                //defaultPositions[i] = legIKTargets[i].localPosition;
                currentPositions[i] = legIKTargets[i].position;
                desiredPositions[i] = legIKTargets[i].position;
                previousPositions[i] = legIKTargets[i].position;
                isMoving[i] = false;
            }
        }

        void Start()
        {

        }

        void FixedUpdate()
        {
            velocity = transform.position - lastBodyPosition;

            if (velocity.magnitude < 0.0005f)
                velocity = lastVelocity;
            else
                lastVelocity = velocity;

            int indexToMove = -1;
            for (int i = 0; i < legIKTargets.Length; i++)
            {
                legIKTargets[i].position = currentPositions[i];

                groundFinderRay = new Ray(body.position + raycastOffsets[i], Vector3.down);
                if (Physics.Raycast(groundFinderRay, out hit, raycastDistance, groundMask))
                {
                    if (Vector3.Distance(hit.point, desiredPositions[i]) > stepLength)
                    {
                        desiredPositions[i] = hit.point;
                        indexToMove = i;
                    }
                }
            }

            for (int i = 0; i < legIKTargets.Length; ++i)
            {
                if (i != indexToMove)
                    legIKTargets[i].position = previousPositions[i];
            }

            if (indexToMove != -1 && !isMoving[0])
            {
                Vector3 targetPoint = desiredPositions[indexToMove] + Mathf.Clamp(velocity.magnitude, 0.0f, 1.5f) * (desiredPositions[indexToMove] - legIKTargets[indexToMove].position) + velocity;
                isMoving[0] = true;
                StartCoroutine(TakeStep(indexToMove, targetPoint));
            }

            lastBodyPosition = body.position;
        }

        IEnumerator TakeStep(int indexOfLegToMove, Vector3 targetPosition)
        {
            Vector3 startPos = previousPositions[indexOfLegToMove];
            for (stepProgress = 0; stepProgress < 1; stepProgress += Time.deltaTime * stepSpeed)
            {
                legIKTargets[indexOfLegToMove].position = Vector3.Lerp(startPos, targetPosition, stepProgress);
                legIKTargets[indexOfLegToMove].position += transform.up * Mathf.Sin(stepProgress * Mathf.PI) * stepHeight;

                yield return new WaitForFixedUpdate();
            }
            legIKTargets[indexOfLegToMove].position = targetPosition;
            previousPositions[indexOfLegToMove] = legIKTargets[indexOfLegToMove].position;
            isMoving[indexOfLegToMove] = false;
        }

        private void OnDrawGizmos()
        {
            for (int i = 0; i < legIKTargets.Length; i++)
            {
                if (legIKTargets != null)
                {
                    //Leg IK Target
                    Gizmos.color = new Color(0, 0, 1, 0.5f); //Transparent Blue
                    Gizmos.DrawCube(legIKTargets[i].position, Vector3.one * 0.025f);

                    //Leg Safe Area
                    Gizmos.color = new Color(1, 0, 1); //Purple
                    Gizmos.DrawWireSphere(legIKTargets[i].position, stepLength);
                }

                if (desiredPositions != null)
                {
                    Gizmos.color = Color.red;
                    Gizmos.DrawRay(groundFinderRay);
                    if (Application.isPlaying)
                        Gizmos.DrawSphere(desiredPositions[i], 0.05f);
                }
            }
        }*/
        #endregion

        #region Capstone
        public Leg[] legs;

        public float stepLength;
        public float stepHeight;
        public float stepSpeed;
        [Space]
        public float rayHeight;
        public float rayLength;
        [Space]
        public Transform body;

        private void Awake()
        {
            legs = GetComponentsInChildren<Leg>();
        }
        #endregion
    }
}