using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Ac1dwir3.PlayerControls;
using UnityEngine.Animations.Rigging;

namespace Ac1dwir3.ProceduralAnimation
{
    public class Leg : MonoBehaviour
    {
        [SerializeField] InsectoidLegSolver solver;

        public Transform ikTargetTransform;
        [ReadOnly] public Vector3 previousPosition;
        public Transform stepTarget;
        [ReadOnly] public bool isStepping = false;

        Ray ray;
        RaycastHit hit;
        [SerializeField] LayerMask groundLayer;

        [SerializeField] Leg legAhead;
        [SerializeField] Leg legAcross;
        bool canStep;

        public enum Side
        {
            Left,
            Right,
        }
        public Side side;

        private void Awake()
        {
            solver = GetComponentInParent<InsectoidLegSolver>();
            previousPosition = ikTargetTransform.position;
        }

        void Update()
        {
            ikTargetTransform.position = previousPosition;

            ray = new Ray(stepTarget.position + new Vector3(0, solver.body.position.y + solver.rayHeight, 0), Vector3.down);

            if (Physics.Raycast(ray, out hit, solver.rayLength, groundLayer))
            {
                stepTarget.position = hit.point;
            }

            if ((legAhead != null && legAhead.isStepping) || (legAcross != null && legAcross.isStepping))
                canStep = false;
            else
                canStep = true;

            if (Vector3.Distance(previousPosition, stepTarget.position) >= solver.stepLength)
            {
                if (canStep && !isStepping)
                {
                    Vector3 stepOverShoot = (stepTarget.position - previousPosition) * GetComponentInParent<PlayerController>().controller.velocity.magnitude;
                    stepOverShoot.y = 0f;

                    StartCoroutine(TakeStep(ikTargetTransform.position, stepTarget.position + stepOverShoot));
                }
            }

        }

        IEnumerator TakeStep(Vector3 startingPosition, Vector3 targetPosition)
        {
            for (float stepProgress = 0; stepProgress < 1; stepProgress += Time.deltaTime * solver.stepSpeed)
            {
                isStepping = true;
                previousPosition = Vector3.Lerp(startingPosition, targetPosition, stepProgress);
                previousPosition += transform.up * Mathf.Sin(stepProgress * Mathf.PI) * solver.stepHeight;

                yield return new WaitForFixedUpdate();
            }

            previousPosition = targetPosition;
            isStepping = false;
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawLine(ray.origin, ray.origin + Vector3.down * solver.rayLength);

            Gizmos.DrawWireSphere(stepTarget.position, .1f);

            Gizmos.color = Color.red;
            if (solver != null)
                Gizmos.DrawWireSphere(previousPosition, solver.stepLength);
        }
    }
}